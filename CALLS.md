## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Nokia NSP Device Configurator. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Nokia NSP Device Configurator.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Nokia_nsp_device_configurator. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAuthBearerToken(body, callback)</td>
    <td style="padding:15px">Get Auth Bearer Token</td>
    <td style="padding:15px">{base_path}/{version}/rest-gateway/rest/api/v1/auth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettheRESTCONFRoot(callback)</td>
    <td style="padding:15px">1. Get the RESTCONF Root</td>
    <td style="padding:15px">{base_path}/{version}/.well-known/host-meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getthedatastore(callback)</td>
    <td style="padding:15px">2. Get the data store</td>
    <td style="padding:15px">{base_path}/{version}/restconf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettheyangLibrary(callback)</td>
    <td style="padding:15px">3. Get the yang-library</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/ietf-yang-library:yang-library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettheschemamounts(callback)</td>
    <td style="padding:15px">4. Get the schema mounts</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/ietf-yang-schema-mount:schema-mounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getallthenetworkdevices(callback)</td>
    <td style="padding:15px">5. Get all the network devices</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getagivennetworkdevice(neId, callback)</td>
    <td style="padding:15px">6. Get a given network device</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMDA(neId, cardId, body, callback)</td>
    <td style="padding:15px">Create MDA</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureMDA(neId, cardId, mdaId, body, callback)</td>
    <td style="padding:15px">Configure MDA</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMDA(neId, cardId, mdaId, callback)</td>
    <td style="padding:15px">Delete MDA</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:/configure/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterface(neId, router, interfaceParam, body, callback)</td>
    <td style="padding:15px">Create Interface</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getthelistofinterfaces(neId, router, callback)</td>
    <td style="padding:15px">Get the list of interfaces</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyyangDataJsoninAcceptPatchheader(neId, router, callback)</td>
    <td style="padding:15px">Verify yang-data+json in Accept-Patch header</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchMultipleinterfaces(neId, router, body, callback)</td>
    <td style="padding:15px">Patch Multiple interfaces</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEmptytype(jneId, interfaceParam, body, callback)</td>
    <td style="padding:15px">Add - empty type</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/junos-conf-root:configuration/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeEmptytype(jneId, interfaceParam, callback)</td>
    <td style="padding:15px">Remove - empty type</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/junos-conf-root:configuration/interfaces/{pathv2}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fieldquerytoselectmultiplefieldsunderthetargetresource(fields, neId, callback)</td>
    <td style="padding:15px">Field query to select multiple fields under the target resource</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:/configure/port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fieldquerywithsubSelectorsofanodeunderthetargetresource(fields, neId, cardId, callback)</td>
    <td style="padding:15px">Field query with sub-selectors of a node under the target resource</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:/configure/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fieldQueryToRetrieveASingleChildNodeUnderTheTargetResource(fields, neId, port, callback)</td>
    <td style="padding:15px">Field query to retrieve a single child node under the target resource</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:/configure/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQoS(networkDevice, callback)</td>
    <td style="padding:15px">Get QoS</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQOSPolicy(neId, body, callback)</td>
    <td style="padding:15px">Create QOS Policy</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQOSSAPEgressPolicy(networkDevice, callback)</td>
    <td style="padding:15px">Get QOS SAP Egress Policy</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/qos/sap-egress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editQOSPolicy(networkDevice, body, callback)</td>
    <td style="padding:15px">Edit QOS Policy</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/network-device-mgr:network-devices/{pathv1}/root/nokia-conf:configure/qos/sap-egress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
