# Nokia_nsp_device_configurator

Vendor: Nokia
Homepage: https://www.nokia.com/

Product: NSP - Device Configurator
Product Page: https://www.nokia.com/networks/ip-networks/network-services-platform/

## Introduction
We classify Nokia NSP Device Configurator into the SD-WAN/SASE domain as Nokia NSP Device Configurator provides a Software Defined Wide Area Network (SD-WAN) solution. 

"Our Network Services Platform (NSP) helps you automate your IP, optical and microwave networks to simplify your operations, respond quickly to fast-changing demand, get the most from your resources and ensure maximum service performance and reliability." 

## Why Integrate
The Nokia NSP Device Configurator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia's NSP Device Configurator to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. 

With this adapter you have the ability to perform operations with Nokia NSP Device Configurator such as:

- QOS
- Search
- Device

## Additional Product Documentation
The [API documents for Nokia NSP](https://documentation.nokia.com/cgi-bin/dbaccessfilename.cgi/3HE12075AAAATQZZA01_V1_NSP%2017.3%20API%20Programmer%20Guide.pdf)
