
## 0.3.5 [10-14-2024]

* Changes made at 2024.10.14_19:33PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_device_configurator!15

---

## 0.3.4 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nokia_nsp_device_configurator!13

---

## 0.3.3 [08-14-2024]

* Changes made at 2024.08.14_17:35PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_device_configurator!12

---

## 0.3.2 [08-06-2024]

* Changes made at 2024.08.06_18:32PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_device_configurator!11

---

## 0.3.1 [05-30-2024]

* Updated incorrect call

See merge request itentialopensource/adapters/adapter-nokia_nsp_device_configurator!10

---

## 0.3.0 [05-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!9

---

## 0.2.3 [03-26-2024]

* Changes made at 2024.03.26_14:06PM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!8

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_10:41AM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!7

---

## 0.2.1 [02-26-2024]

* Changes made at 2024.02.26_13:03PM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!6

---

## 0.2.0 [01-02-2024]

* Adapter Migration

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!5

---

## 0.1.5 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_device_configurator!4

---

## 0.1.4 [04-18-2023]

* Bug fixes and performance improvements

See commit eb3b6fc

---

## 0.1.3 [04-18-2023]

* Bug fixes and performance improvements

See commit 8aa8777

---

## 0.1.2 [04-18-2023]

* Bug fixes and performance improvements

See commit ca5cffc

---

## 0.1.1 [04-18-2023]

* Bug fixes and performance improvements

See commit ba9838c

---
