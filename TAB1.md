# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Nokia_nsp_device_configurator System. The API that was used to build the adapter for Nokia_nsp_device_configurator is usually available in the report directory of this adapter. The adapter utilizes the Nokia_nsp_device_configurator API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Nokia NSP Device Configurator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia's NSP Device Configurator to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. 

With this adapter you have the ability to perform operations with Nokia NSP Device Configurator such as:

- QOS
- Search
- Device

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
